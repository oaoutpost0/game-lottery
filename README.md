### How to Reproduce the Results of a Lottery ###
This code is the template that is used to determine who wins the game lotteries. It is written in R.
It is fairly easy to reproduce the results of a lottery. First, you need an environment where you can run the R script.

* [Download and Install R](https://cloud.r-project.org/)
* Optionally, after installing R, you can download and install [Rstudio](https://rstudio.com/products/rstudio/download/) for a friendly user experience
* Alternatively, if you don't want to download and install anything, you can use an [online R interpreter](https://www.tutorialspoint.com/execute_r_online.php)

Once you have installed R or opened an online R interpreter, open the file `game lottery.R` in the program or browser window.
If you installed R without installing Rstudio, search for an application called `Rgui`. If you installed Rstudio, open `Rstudio`.
Make sure you install the 64 bit version of R. Reproducibility is not guaranteed if you use the 32 bit version (this is not because the 32 bit version is bugged, but because I am using the 64 bit version).

Next, you need to modify the R script in 3 ways:

1. On line 10 of the script, add every person who participated in the lottery. An example is provided in the code. Make sure each name is surrounded by quotes and that they are separated by commas.
2. On line 12 of the script, add every game that was available in the lottery. Fill it out the same was as you did the people.
3. On line 5 of the script, remove the `#` and replace the number with the random seed that was provided in the lottery results.

At this point, all the changes you need to make are complete. All you have to do now is run your script.

1. If you are using just plain R, select the entire script, then click the icon in the upper left that says "Run line or selection" when you hover your mouse over the icon.
2. If you are using Rstudio, click the `Source` button in the upper right of the script edit window.
3. If you are using the online R interpreted linked above, click the `Execute` button in the top left of the screen.
